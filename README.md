# FakeSH - a fake shell program

This program behaves like a POSIX shell without executing any commands.

![screenshot](screenshot.png)

## Dependencies

The following are required:

* A Linux or BSD operating system
* Python 3.9 or higher
* The `blessed` Python library ([link](https://pypi.org/project/blessed/))

## Usage

* If cloned: `./fakesh`
* If installed: `fakesh`

There are no user-configurable settings or arguments.

## Installing

1. Ensure the [dependencies](#dependencies) are satisfied
2. Run the install script: `make install`

## Uninstalling

1. Run the uninstall script: `make uninstall`
2. Remove unwanted [dependencies](#dependencies) (optional)

**NOTE**: This program will leave log files at `~/.local/share/fakesh` . You may want to delete this directory after running the uninstall script.
